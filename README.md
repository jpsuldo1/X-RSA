# X-RSA 

**What's X-RSA ?**
- it's a Tool Which contains a many of attack types in RSA such as Hasted, Common Modulus, Chinese Remainder Theorem, Wiener ... etc , and it's still under development and adding other Attacks.

**What's New in X-RSA ?**
- Added [ New Attacks , Factorization Methods ] To X-RSA V0.3  And Fixing Many Error in V0.2

**Why X-RSA ?**
- X-RSA help you in [CTF , Penetration Testing , Decryption]

- Written By [ Python 2.7 ]


# Installing and Running Tool 
1 - Install Tool 
```
git clone https://github.com/X-Vector/X-RSA.git
```
2 - Download Requirement
```
apt install libgmp-dev libmpfr-dev libmpc-dev
pip install -r requirement.txt
```
3 - Run Tool 
```
python Attack.py
```
# Screenshots
**1 - X-RSA**
![X-RSA](https://2.top4top.net/p_1265rgruu1.png)

**2 - X-RSA Attackes**
![X-RSA](https://3.top4top.net/p_1265d35pz2.png)

**3 - X-RSA Factorization**
![X-RSA](https://4.top4top.net/p_12653eztj3.png)

- Coded By X-Vector
- [Facebook](https://www.facebook.com/X.Vector1) - [Linkedin](https://www.linkedin.com/in/x-vector/) - [Twitter](https://twitter.com/@XVector11)

# Reference
- [Quadratic sieve](https://github.com/skollmann/PyFactorise)
